package ru.t1.nikitushkina.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.nikitushkina.tm.dto.request.UserChangePasswordRequest;
import ru.t1.nikitushkina.tm.enumerated.Role;
import ru.t1.nikitushkina.tm.util.TerminalUtil;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "change-user-pwd";

    @NotNull
    public static final String DESCRIPTION = "Change user password.";

    @Override
    public void execute() {
        System.out.println("[CHANGE PASSWORD]");
        System.out.print("NEW PASSWORD: ");
        @NotNull final String newPassword = TerminalUtil.nextLine();
        @NotNull final UserChangePasswordRequest request = new UserChangePasswordRequest();
        request.setPassword(newPassword);
        getUserEndpointClient().changeUserPassword(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
