package ru.t1.nikitushkina.tm.command.system;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.api.service.ICommandService;
import ru.t1.nikitushkina.tm.api.service.IPropertyService;
import ru.t1.nikitushkina.tm.command.AbstractCommand;
import ru.t1.nikitushkina.tm.enumerated.Role;

@Getter
@Setter
public abstract class AbstractSystemCommand extends AbstractCommand {

    @NotNull
    protected ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

    @NotNull
    protected IPropertyService getPropertyService() {
        return serviceLocator.getPropertyService();
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
