package ru.t1.nikitushkina.tm.command.server;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.command.AbstractCommand;
import ru.t1.nikitushkina.tm.enumerated.Role;

public final class DisconnectCommand extends AbstractCommand {

    @NotNull
    public static final String NAME = "disconnect";

    @NotNull
    public static final String DESCRIPTION = "Disconnect from server.";

    @Override
    @SneakyThrows
    public void execute() {
        try {
            getServiceLocator().getConnectionEndpointClient().disconnect();
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
        }
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[0];
    }

}
