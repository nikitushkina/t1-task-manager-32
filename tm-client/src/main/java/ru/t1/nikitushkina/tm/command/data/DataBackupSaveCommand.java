package ru.t1.nikitushkina.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.dto.request.DataBackupSaveRequest;
import ru.t1.nikitushkina.tm.enumerated.Role;

public class DataBackupSaveCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-backup";

    @NotNull
    public static final String DESCRIPTION = "Backup data in xml file.";

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull DataBackupSaveRequest request = new DataBackupSaveRequest();
        getDomainEndpointClient().saveDataBackup(request);
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{
                Role.ADMIN
        };
    }

}
