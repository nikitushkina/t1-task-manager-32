package ru.t1.nikitushkina.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.dto.request.ProjectUpdateByIndexRequest;
import ru.t1.nikitushkina.tm.util.TerminalUtil;

public final class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-update-by-index";

    @NotNull
    public static final String DESCRIPTION = "Update project by index.";

    @Override
    public void execute() {
        System.out.println("[UPDATE PROJECT BY INDEX]");
        System.out.print("ENTER INDEX: ");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.print("ENTER NAME: ");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.print("ENTER DESCRIPTION: ");
        @NotNull final String description = TerminalUtil.nextLine();
        @Nullable final ProjectUpdateByIndexRequest request = new ProjectUpdateByIndexRequest();
        request.setName(name);
        request.setDescription(description);
        request.setIndex(index);
        getProjectEndpointClient().updateByIndex(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
