package ru.t1.nikitushkina.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.api.service.IPropertyService;
import ru.t1.nikitushkina.tm.dto.request.ServerAboutRequest;
import ru.t1.nikitushkina.tm.dto.response.ServerAboutResponse;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @NotNull
    public static final String NAME = "about";

    @NotNull
    public static final String DESCRIPTION = "Display developer info.";

    @NotNull
    public static final String ARGUMENT = "-a";

    @Override
    public void execute() {
        @NotNull final IPropertyService service = getPropertyService();
        System.out.println("[ABOUT]");
        System.out.println("[Client]");
        System.out.println("Name:  " + service.getAuthorName());
        System.out.println("E-mail: " + service.getAuthorEmail());
        System.out.println();
        System.out.println("[GIT]");
        System.out.println("branch: " + service.getGitBranch());
        System.out.println("commit id: " + service.getCommitId());
        System.out.println("commit time: " + service.getCommitTime());
        System.out.println("commit message: " + service.getCommitMsgFull());
        System.out.println("committer name: " + service.getCommitterName());
        System.out.println("committer email: " + service.getCommitterEmail());
        System.out.println();
        System.out.println("[Server]");
        @Nullable final ServerAboutRequest request = new ServerAboutRequest();
        @Nullable final ServerAboutResponse response = serviceLocator.getSystemEndpointClient().getAbout(request);
        System.out.println("Name: " + response.getName());
        System.out.println("E-mail: " + response.getEmail());
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
