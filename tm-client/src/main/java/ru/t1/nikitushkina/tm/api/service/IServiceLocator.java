package ru.t1.nikitushkina.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.nikitushkina.tm.api.client.IEndpointClient;
import ru.t1.nikitushkina.tm.client.*;

public interface IServiceLocator {

    @NotNull
    AuthEndpointClient getAuthEndpointClient();

    @NotNull
    ICommandService getCommandService();

    @NotNull
    IEndpointClient getConnectionEndpointClient();

    @NotNull
    DomainEndpointClient getDomainEndpointClient();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    ProjectEndpointClient getProjectEndpointClient();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    SystemEndpointClient getSystemEndpointClient();

    @NotNull
    TaskEndpointClient getTaskEndpointClient();

    @NotNull
    UserEndpointClient getUserEndpointClient();

}
