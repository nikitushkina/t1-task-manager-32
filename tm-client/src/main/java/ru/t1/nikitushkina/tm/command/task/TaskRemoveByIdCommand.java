package ru.t1.nikitushkina.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.dto.request.TaskRemoveByIdRequest;
import ru.t1.nikitushkina.tm.util.TerminalUtil;

public final class TaskRemoveByIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-remove-by-id";

    @NotNull
    public static final String DESCRIPTION = "Remove task by id.";

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.print("ENTER ID: ");
        @NotNull final String id = TerminalUtil.nextLine();
        @Nullable final TaskRemoveByIdRequest request = new TaskRemoveByIdRequest();
        request.setId(id);
        getTaskEndpointClient().removeById(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
