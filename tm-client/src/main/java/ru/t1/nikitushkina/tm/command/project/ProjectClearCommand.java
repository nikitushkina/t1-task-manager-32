package ru.t1.nikitushkina.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.nikitushkina.tm.dto.request.ProjectCreateRequest;

public final class ProjectClearCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-clear";

    @NotNull
    public static final String DESCRIPTION = "Remove all projects.";

    @Override
    public void execute() {
        System.out.println("[CLEAR PROJECTS]");
        @NotNull ProjectCreateRequest request = new ProjectCreateRequest();
        getProjectEndpointClient().createProject(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
