package ru.t1.nikitushkina.tm.client;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.nikitushkina.tm.api.endpoint.IAuthEndpoint;
import ru.t1.nikitushkina.tm.dto.request.UserLoginRequest;
import ru.t1.nikitushkina.tm.dto.request.UserLogoutRequest;
import ru.t1.nikitushkina.tm.dto.request.UserProfileRequest;
import ru.t1.nikitushkina.tm.dto.response.UserLoginResponse;
import ru.t1.nikitushkina.tm.dto.response.UserLogoutResponse;
import ru.t1.nikitushkina.tm.dto.response.UserProfileResponse;

@Getter
@Setter
@NoArgsConstructor
public final class AuthEndpointClient extends AbstractEndpointClient implements IAuthEndpoint {

    public AuthEndpointClient(@NotNull AbstractEndpointClient client) {
        super(client);
    }

    @SneakyThrows
    public static void main(String[] args) {
        @NotNull final AuthEndpointClient authEndpointClient = new AuthEndpointClient();
        authEndpointClient.connect();
        System.out.println(authEndpointClient.profile(new UserProfileRequest()).getUser());

        System.out.println(authEndpointClient.login(new UserLoginRequest("user2", "user2pwd")).getSuccess());
        System.out.println(authEndpointClient.profile(new UserProfileRequest()).getUser());

        System.out.println(authEndpointClient.login(new UserLoginRequest("user1", "user1pwd")).getSuccess());
        System.out.println(authEndpointClient.profile(new UserProfileRequest()).getUser().getEmail());
        System.out.println(authEndpointClient.logout(new UserLogoutRequest()));

        System.out.println(authEndpointClient.profile(new UserProfileRequest()).getUser());
        authEndpointClient.disconnect();
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserLoginResponse login(@NotNull final UserLoginRequest request) {
        return call(request, UserLoginResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserLogoutResponse logout(@NotNull final UserLogoutRequest request) {
        return call(request, UserLogoutResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserProfileResponse profile(@NotNull final UserProfileRequest request) {
        return call(request, UserProfileResponse.class);
    }

}
