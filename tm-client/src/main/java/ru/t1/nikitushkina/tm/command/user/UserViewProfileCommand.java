package ru.t1.nikitushkina.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.dto.request.UserProfileRequest;
import ru.t1.nikitushkina.tm.dto.response.UserProfileResponse;
import ru.t1.nikitushkina.tm.enumerated.Role;
import ru.t1.nikitushkina.tm.model.User;

public final class UserViewProfileCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "view-user-profile";

    @NotNull
    public static final String DESCRIPTION = "View profile of current user.";

    @Override
    public void execute() {
        @NotNull final UserProfileRequest request = new UserProfileRequest();
        @Nullable final UserProfileResponse response = getAuthEndpointClient().profile(request);
        @NotNull final User user = response.getUser();
        showUser(user);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
