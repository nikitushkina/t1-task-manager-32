package ru.t1.nikitushkina.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.nikitushkina.tm.dto.request.*;
import ru.t1.nikitushkina.tm.dto.response.*;

public interface IUserEndpoint {

    @NotNull
    UserChangePasswordResponse changeUserPassword(@NotNull final UserChangePasswordRequest request);

    @NotNull
    UserLockResponse lockUser(@NotNull final UserLockRequest request);

    @NotNull
    UserRegistryResponse registryUser(@NotNull final UserRegistryRequest request);

    @NotNull
    UserRemoveResponse removeUser(@NotNull final UserRemoveRequest request);

    @NotNull
    UserUnlockResponse unlockUser(@NotNull final UserUnlockRequest request);

    @NotNull
    UserUpdateProfileResponse updateUserProfile(@NotNull final UserUpdateProfileRequest request);

}
