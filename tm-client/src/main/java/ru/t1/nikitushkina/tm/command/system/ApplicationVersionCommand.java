package ru.t1.nikitushkina.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.dto.request.ServerVersionRequest;
import ru.t1.nikitushkina.tm.dto.response.ServerVersionResponse;

public final class ApplicationVersionCommand extends AbstractSystemCommand {

    @NotNull
    public static final String NAME = "version";

    @NotNull
    public static final String DESCRIPTION = "Display application version.";

    @NotNull
    public static final String ARGUMENT = "-v";

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.print("Client: ");
        System.out.println(getPropertyService().getApplicationVersion());
        @Nullable final ServerVersionRequest request = new ServerVersionRequest();
        @Nullable final ServerVersionResponse response = serviceLocator.getSystemEndpointClient().getVersion(request);
        System.out.println("Server: " + response.getVersion());
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
