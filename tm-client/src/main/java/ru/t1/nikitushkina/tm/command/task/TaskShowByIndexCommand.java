package ru.t1.nikitushkina.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.dto.request.TaskGetByIndexRequest;
import ru.t1.nikitushkina.tm.dto.response.TaskGetByIndexResponse;
import ru.t1.nikitushkina.tm.model.Task;
import ru.t1.nikitushkina.tm.util.TerminalUtil;

public final class TaskShowByIndexCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-show-by-index";

    @NotNull
    public static final String DESCRIPTION = "Show task by index.";

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.print("ENTER INDEX: ");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @Nullable final TaskGetByIndexRequest request = new TaskGetByIndexRequest();
        request.setIndex(index);
        @Nullable final TaskGetByIndexResponse response = getTaskEndpointClient().getTaskByIndex(request);
        @Nullable final Task task = response.getTask();
        showTask(task);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
