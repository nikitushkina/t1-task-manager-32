package ru.t1.nikitushkina.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.nikitushkina.tm.dto.request.UserLoginRequest;
import ru.t1.nikitushkina.tm.dto.request.UserLogoutRequest;
import ru.t1.nikitushkina.tm.dto.request.UserProfileRequest;
import ru.t1.nikitushkina.tm.dto.response.UserLoginResponse;
import ru.t1.nikitushkina.tm.dto.response.UserLogoutResponse;
import ru.t1.nikitushkina.tm.dto.response.UserProfileResponse;

public interface IAuthEndpoint {

    @NotNull
    @SneakyThrows
    UserLoginResponse login(@NotNull UserLoginRequest request);

    @NotNull
    @SneakyThrows
    UserLogoutResponse logout(@NotNull UserLogoutRequest request);

    @NotNull
    @SneakyThrows
    UserProfileResponse profile(@NotNull UserProfileRequest request);

}
