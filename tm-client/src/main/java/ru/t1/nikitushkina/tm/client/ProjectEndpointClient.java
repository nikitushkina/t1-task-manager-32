package ru.t1.nikitushkina.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.nikitushkina.tm.api.endpoint.IProjectEndpoint;
import ru.t1.nikitushkina.tm.dto.request.*;
import ru.t1.nikitushkina.tm.dto.response.*;

@NoArgsConstructor
public final class ProjectEndpointClient extends AbstractEndpointClient implements IProjectEndpoint {

    public ProjectEndpointClient(@NotNull AbstractEndpointClient client) {
        super(client);
    }

    @SneakyThrows
    public static void main(String[] args) {
        @NotNull final AuthEndpointClient authEndpointClient = new AuthEndpointClient();
        authEndpointClient.connect();
        System.out.println(authEndpointClient.login(new UserLoginRequest("test", "test")).getSuccess());
        System.out.println(authEndpointClient.profile(new UserProfileRequest()).getUser().getEmail());

        @NotNull final ProjectEndpointClient projectClient = new ProjectEndpointClient(authEndpointClient);
        System.out.println(projectClient.createProject(new ProjectCreateRequest("HELLO", "WORLD")));
        System.out.println(projectClient.listProject(new ProjectListRequest()).getProjects());

        System.out.println(authEndpointClient.logout(new UserLogoutRequest()));
        authEndpointClient.disconnect();
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectChangeStatusByIdResponse changeProjectStatusById(@NotNull final ProjectChangeStatusByIdRequest request) {
        return call(request, ProjectChangeStatusByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectChangeStatusByIndexResponse changeProjectStatusByIndex(@NotNull final ProjectChangeStatusByIndexRequest request) {
        return call(request, ProjectChangeStatusByIndexResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectClearResponse clearProject(@NotNull final ProjectClearRequest request) {
        return call(request, ProjectClearResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectCompleteByIdResponse completeById(@NotNull final ProjectCompleteByIdRequest request) {
        return call(request, ProjectCompleteByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectCompleteByIndexResponse completeByIndex(@NotNull final ProjectCompleteByIndexRequest request) {
        return call(request, ProjectCompleteByIndexResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectCreateResponse createProject(@NotNull final ProjectCreateRequest request) {
        return call(request, ProjectCreateResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectGetByIdResponse getProjectById(@NotNull final ProjectGetByIdRequest request) {
        return call(request, ProjectGetByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectGetByIndexResponse getProjectByIndex(@NotNull final ProjectGetByIndexRequest request) {
        return call(request, ProjectGetByIndexResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectListResponse listProject(@NotNull final ProjectListRequest request) {
        return call(request, ProjectListResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectRemoveByIdResponse removeById(@NotNull final ProjectRemoveByIdRequest request) {
        return call(request, ProjectRemoveByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectRemoveByIndexResponse removeByIndex(@NotNull final ProjectRemoveByIndexRequest request) {
        return call(request, ProjectRemoveByIndexResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectShowByIdResponse showById(@NotNull final ProjectShowByIdRequest request) {
        return call(request, ProjectShowByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectShowByIndexResponse showByIndex(@NotNull final ProjectShowByIndexRequest request) {
        return call(request, ProjectShowByIndexResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectStartByIdResponse startById(@NotNull final ProjectStartByIdRequest request) {
        return call(request, ProjectStartByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectStartByIndexResponse startByIndex(@NotNull final ProjectStartByIndexRequest request) {
        return call(request, ProjectStartByIndexResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectUpdateByIdResponse updateById(@NotNull final ProjectUpdateByIdRequest request) {
        return call(request, ProjectUpdateByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectUpdateByIndexResponse updateByIndex(@NotNull final ProjectUpdateByIndexRequest request) {
        return call(request, ProjectUpdateByIndexResponse.class);
    }

}
