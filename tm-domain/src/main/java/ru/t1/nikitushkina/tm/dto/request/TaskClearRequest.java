package ru.t1.nikitushkina.tm.dto.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TaskClearRequest extends AbstractUserRequest {
}
