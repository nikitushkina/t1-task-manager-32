package ru.t1.nikitushkina.tm.dto.response;

import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.model.Project;

public class ProjectCreateResponse extends AbstractProjectResponse {

    public ProjectCreateResponse(@Nullable Project project) {
        super(project);
    }

}
