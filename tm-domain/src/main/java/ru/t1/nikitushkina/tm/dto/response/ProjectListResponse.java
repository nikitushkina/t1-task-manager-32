package ru.t1.nikitushkina.tm.dto.response;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.model.Project;

import java.util.List;

@Getter
@Setter
public class ProjectListResponse extends AbstractResponse {

    @Nullable
    private List<Project> projects;

    public ProjectListResponse(@Nullable final List<Project> projects) {
        this.projects = projects;
    }

}
