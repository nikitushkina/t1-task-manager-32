package ru.t1.nikitushkina.tm.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.enumerated.Status;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProjectChangeStatusByIdRequest extends AbstractUserRequest {

    @Nullable
    private String projectId;

    @Nullable
    private Status status;

}
