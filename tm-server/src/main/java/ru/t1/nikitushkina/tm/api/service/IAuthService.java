package ru.t1.nikitushkina.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.enumerated.Role;
import ru.t1.nikitushkina.tm.model.User;

public interface IAuthService {

    @NotNull
    User check(@Nullable String login, @Nullable String password);

    @NotNull
    User registry(@Nullable String login, @Nullable String password, @Nullable String email);

}
