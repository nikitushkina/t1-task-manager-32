package ru.t1.nikitushkina.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.dto.Domain;

public interface IDomainService {

    @NotNull Domain getDomain();

    void setDomain(@Nullable Domain domain);

    void loadDataBackup();

    void loadDataBase64();

    void loadDataBinary();

    void loadDataJsonFasterXml();

    void loadDataJsonJaxB();

    void loadDataXmlFasterXml();

    void loadDataXmlJaxB();

    void loadDataYamlFasterXml();

    void saveDataBackup();

    void saveDataBase64();

    void saveDataBinary();

    void saveDataJsonFasterXml();

    void saveDataJsonJaxB();

    void saveDataXmlFasterXml();

    void saveDataXmlJaxB();

    void saveDataYamlFasterXml();

}
