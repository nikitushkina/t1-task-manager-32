package ru.t1.nikitushkina.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.nikitushkina.tm.dto.request.*;
import ru.t1.nikitushkina.tm.dto.response.*;

public interface IProjectEndpoint {

    @NotNull
    ProjectChangeStatusByIdResponse changeProjectStatusById(@NotNull ProjectChangeStatusByIdRequest request);

    @NotNull
    ProjectChangeStatusByIndexResponse changeProjectStatusByIndex(@NotNull ProjectChangeStatusByIndexRequest request);

    @NotNull
    ProjectClearResponse clearProject(@NotNull ProjectClearRequest request);

    @NotNull
    ProjectCompleteByIdResponse completeById(@NotNull ProjectCompleteByIdRequest request);

    @NotNull
    ProjectCompleteByIndexResponse completeByIndex(@NotNull ProjectCompleteByIndexRequest request);

    @NotNull
    ProjectCreateResponse createProject(@NotNull ProjectCreateRequest request);

    @NotNull
    ProjectGetByIdResponse getProjectById(@NotNull ProjectGetByIdRequest request);

    @NotNull
    ProjectGetByIndexResponse getProjectByIndex(@NotNull ProjectGetByIndexRequest request);

    @NotNull
    ProjectListResponse listProject(@NotNull ProjectListRequest request);

    @NotNull
    ProjectRemoveByIdResponse removeById(@NotNull ProjectRemoveByIdRequest request);

    @NotNull
    ProjectRemoveByIndexResponse removeByIndex(@NotNull ProjectRemoveByIndexRequest request);

    @NotNull
    ProjectShowByIdResponse showById(@NotNull ProjectShowByIdRequest request);

    @NotNull
    ProjectShowByIndexResponse showByIndex(@NotNull ProjectShowByIndexRequest request);

    @NotNull
    ProjectStartByIdResponse startById(@NotNull ProjectStartByIdRequest request);

    @NotNull
    ProjectStartByIndexResponse startByIndex(@NotNull ProjectStartByIndexRequest request);

    @NotNull
    ProjectUpdateByIdResponse updateById(@NotNull ProjectUpdateByIdRequest request);

    @NotNull
    ProjectUpdateByIndexResponse updateByIndex(@NotNull ProjectUpdateByIndexRequest request);

}
