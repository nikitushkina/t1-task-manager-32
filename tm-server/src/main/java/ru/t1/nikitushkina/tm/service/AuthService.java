package ru.t1.nikitushkina.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.api.service.IAuthService;
import ru.t1.nikitushkina.tm.api.service.IPropertyService;
import ru.t1.nikitushkina.tm.api.service.IUserService;
import ru.t1.nikitushkina.tm.exception.user.LoginEmptyException;
import ru.t1.nikitushkina.tm.exception.user.PasswordEmptyException;
import ru.t1.nikitushkina.tm.model.User;
import ru.t1.nikitushkina.tm.util.HashUtil;

import javax.naming.AuthenticationException;

public class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IPropertyService propertyService;

    public AuthService(
            @NotNull final IPropertyService propertyService,
            @NotNull final IUserService userService
    ) {
        this.userService = userService;
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    public User registry(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        return userService.create(login, password, email);
    }

    @NotNull
    @Override
    @SneakyThrows
    public User check(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new AuthenticationException();
        if (user.getLocked()) throw new AuthenticationException();
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (hash == null) throw new AuthenticationException();
        if (!hash.equals(user.getPasswordHash())) throw new AuthenticationException();
        return user;
    }

}
