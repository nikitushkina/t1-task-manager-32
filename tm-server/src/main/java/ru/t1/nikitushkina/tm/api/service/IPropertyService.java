package ru.t1.nikitushkina.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.nikitushkina.tm.api.component.ISaltProvider;

public interface IPropertyService extends ISaltProvider {

    @NotNull
    String getApplicationConfig();

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getAuthorName();

    @NotNull
    String getCommitId();

    @NotNull
    String getCommitMsgFull();

    @NotNull
    String getCommitTime();

    @NotNull
    String getCommitterEmail();

    @NotNull
    String getCommitterName();

    @NotNull
    String getGitBranch();

    @NotNull
    Integer getServerPort();

}
